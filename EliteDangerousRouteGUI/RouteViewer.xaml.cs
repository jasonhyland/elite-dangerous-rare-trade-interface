﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using System.Windows.Threading;
using System.Threading;

namespace EliteDangerousRouteGUI
{
    /// <summary>
    /// Interaction logic for RouteViewer.xaml
    /// </summary>
    /// 

    public partial class RouteViewer : Window
    {
        public string file;
        public string routeDirectory;
        public string FType;
        public RouteViewer(string directory, string fileType)
        {
            routeDirectory = directory;
            FType = fileType;
            InitializeComponent();            
        }

        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void route_file_list_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DoEvents();
            route_output.Text = "";
            file = System.IO.Path.Combine(routeDirectory, route_file_list.SelectedValue.ToString() + ".markdown");
            Thread t = new Thread(startReadingFile);
            t.IsBackground = true;
            t.Start();
        }

        private void startReadingFile()
        {                         
            
            
            if (File.Exists(file))
                foreach (string line in File.ReadAllLines(file))
                {
                    DoEvents();
                    //route_output.Text = string.Format("{0}\n{1}", route_output.Text, line);
                    setText(route_output, line);
                    if (quit)
                        break;
                }
            else
                MessageBox.Show("This file no longer Exists! Try again.");
        }

        private void minimize_button_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = System.Windows.WindowState.Minimized;
        }

        private void close_button_Click(object sender, RoutedEventArgs e)
        {
            quit = true;
            
            this.Close();            
        }
        public bool quit=false;
        public void DoEvents()
        {
            DispatcherFrame frame = new DispatcherFrame();
            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background,
                new DispatcherOperationCallback(ExitFrame), frame);
            Dispatcher.PushFrame(frame);
        }

        public object ExitFrame(object f)
        {
            ((DispatcherFrame)f).Continue = false;
            return null;
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {

            foreach (string file in Directory.GetFiles(routeDirectory, FType, SearchOption.TopDirectoryOnly))
            {
                route_file_list.Items.Add(System.IO.Path.GetFileNameWithoutExtension(file));
            }
            if (route_file_list.Items.Count > 0)
                route_file_list.SelectedIndex = 0;
            else
                route_output.Text = "No Routes Found, make sure you choose a System that has rare goods.";
        }

        private void route_output_TextChanged(object sender, TextChangedEventArgs e)
        {
            DoEvents();
        }

        private void setText(TextBox text, string line)
        {
            try
            {
                this.Dispatcher.Invoke((Action)(() =>
                {
                    text.Text = string.Format("{0}\n{1}", text.Text, line);
                }));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
