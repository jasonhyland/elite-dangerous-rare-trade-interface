﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using Newtonsoft.Json;
using System.Net;
using System.Windows.Threading;
using System.Reflection;
using System.Diagnostics;
using System.Xml;
using System.Threading;

namespace EliteDangerousRouteGUI
{
    
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static string updateHostURL = "http://edroutegenerator-update.no-ip.org/latest_update.exe";
        public static string hostURL = "http://edroutegenerator-update.no-ip.org/";

        public static string forumPath = "EDForums.com";
        public static string customRubyScript = "EDRoutes.rb";
        public static string SystemList = "system_list.dat";
        public static string rubyWorkingPath = System.IO.Path.Combine(Environment.CurrentDirectory, "Ruby\\bin");
        public static string tmpRubyFile = "generate-routes.tmp";
        public MainWindow()
        {
            if (Properties.Settings.Default.UpdateSettings)
            {
                Properties.Settings.Default.Upgrade();
                Properties.Settings.Default.UpdateSettings = false;
                Properties.Settings.Default.Save();
            }

            InitializeComponent();
            updateThreaded(true);
            version_number.Content = string.Format("v{0}", Assembly.GetExecutingAssembly().GetName().Version);
            if (!System.IO.Directory.Exists(rubyWorkingPath))
                if (File.Exists("Ruby.exe"))
                    startHidden7z("Ruby.exe");
                else
                    MessageBox.Show("Ruby Installation is missing. Please Extract Ruby.exe in the same directory as this application.");



        }

        private void startHidden7z(string p)
        {
            Process P = new Process();
            P.StartInfo.WorkingDirectory = Environment.CurrentDirectory;
            P.StartInfo.FileName = p;
            P.StartInfo.Arguments = string.Format("-o\"{0}\" -y", Environment.CurrentDirectory);
            P.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            P.StartInfo.CreateNoWindow = true;
            P.Start();
            while (!P.HasExited)
            {
                System.Threading.Thread.Sleep(100);
            }            
        }
        
        private void setSystemList()
        {
            using (WebClient webClient = new WebClient())
            {
                System.IO.File.Delete("system_list.dat");
                using (StreamWriter write = new StreamWriter("system_list.dat", true))
                {
                    dynamic result = JsonConvert.DeserializeObject(webClient.DownloadString("https://raw.githubusercontent.com/SteveHodge/ed-systems/master/systems.json"));
                    foreach (var name in result)
                    {
                        Newtonsoft.Json.Linq.JObject jObject = Newtonsoft.Json.Linq.JObject.Parse(name.ToString());
                        write.WriteLine((string)jObject["name"]);
                    }
                }
            }
            
                
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            setPreferences();           
            scriptValues content = createObject();
            if (content != null)
            {
                if (generateRubyScript(content))
                {
                    try
                    {
                        foreach (string file in Directory.GetFiles(System.IO.Path.Combine(rubyWorkingPath, "routes")))
                        {
                            try { File.Delete(file); }
                            catch { }
                        }
                    }
                    catch
                    {

                    }
                    this.Visibility = System.Windows.Visibility.Hidden;
                    ConsoleOutput showCons = new ConsoleOutput(this.Topmost);
                    showCons.Owner = this;
                    showCons.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterOwner;
                    showCons.ShowDialog();
                    this.Visibility = System.Windows.Visibility.Visible;
                }
               
            }
            else
            {
                MessageBox.Show("There was an error with your values. Please try again");                
            }
        }

        public class scriptValues
        {
            public int maxStationDistance;
            public int maxJumpDistance;
            public int minSellDistance;
            public int maxDepth;
            public int maxJumps;
            public int bestCount;
            public int warningMaxWeight;
            public int maxAllowedUndersells;
            public int minRequiredSellDistance;
            public int maxGoodsBeforeOverflow;
            public string start_system;
        }

        public scriptValues createObject()
        {
            try
            {
                scriptValues v = new scriptValues();
                v.maxStationDistance = int.Parse(max_stn_value.Text);
                v.maxJumpDistance = int.Parse(max_jump_value.Text);
                v.minSellDistance = int.Parse(min_sell_distance_value.Text);
                v.maxDepth = int.Parse(max_depth_value.Text);
                v.maxJumps = int.Parse(max_jumps_value.Text);
                v.bestCount = int.Parse(best_count_value.Text);
                v.warningMaxWeight = int.Parse(warning_max_value.Text);
                v.maxAllowedUndersells = int.Parse(max_undersells_value.Text);
                v.minRequiredSellDistance = int.Parse(min_sell_req_distance_value.Text);
                v.maxGoodsBeforeOverflow = int.Parse(max_goods_overflow_value.Text);
                v.start_system = starting_system_value.Text;
                return v;
            }
            catch
            {
                return null;
            }
        }

        public bool generateRubyScript(scriptValues values)
        {
            //string writeDirectory = System.IO.Path.Combine(Environment.CurrentDirectory,"Ruby\\bin");
            //string defaultScript = System.IO.Path.Combine(writeDirectory,"generate-routes.rb");
            string customScriptLocation = System.IO.Path.Combine(writeDirectory, customRubyScript);
            if (File.Exists(customScriptLocation))
                File.Delete(customScriptLocation);
            try
            {
                using (StreamWriter write = new StreamWriter(customScriptLocation, true))
                {
                    foreach (string line in File.ReadAllLines(defaultScript))
                    {
                        if (line == "CONTENTPLACEHOLDER")
                        {
                            write.WriteLine(string.Format("{0} = {1}", "$max_station_dist", values.maxStationDistance));
                            write.WriteLine(string.Format("{0} = {1}", "$max_jump_dist", values.maxJumpDistance));
                            write.WriteLine(string.Format("{0} = {1}", "$min_sell_dist", values.minSellDistance));
                            write.WriteLine(string.Format("{0} = {1}", "$max_depth", values.maxDepth));
                            write.WriteLine(string.Format("{0} = {1}", "$max_jumps", values.maxJumps));
                            write.WriteLine(string.Format("{0} = {1}", "$best_count", values.bestCount));
                            write.WriteLine(string.Format("{0} = {1}", "$warning_max_weight", values.warningMaxWeight));
                            write.WriteLine(string.Format("{0} = {1}", "$max_allowed_undersells", values.maxAllowedUndersells));
                            write.WriteLine(string.Format("{0} = {1}", "$min_required_sell_dist", values.minRequiredSellDistance));
                            write.WriteLine(string.Format("{0} = {1}", "$max_goods_before_overflow", values.maxGoodsBeforeOverflow));
                            write.WriteLine(string.Format("{0} = '{1}'", "$start_system", values.start_system));
                        }
                        else
                            write.WriteLine(line);
                    }

                }
                return true;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            if (!File.Exists(SystemList))
            {
                MessageBox.Show("You do not have a list of systems downloaded. Press OK To download. This may take a moment.");
                refreshSystemList();
            }
            else
                setControlList();
            getPreferences();

        }

        private void getPreferences()
        {
            starting_system_value.SelectedIndex = Properties.Settings.Default.lastSystem;
            
            max_stn_value.Value = Properties.Settings.Default.maxStationDist;
            max_stn_value.ToolTip = "Farthest Station from sun (in Ls)";
            max_jump_value.Value = Properties.Settings.Default.maxJumpDist;
            max_jump_value.ToolTip = "Furthest LY Jump";
            min_sell_distance_value.Value = Properties.Settings.Default.minSellDist;
            min_sell_distance_value.ToolTip = "closest distance to sell the items";
            max_depth_value.Value = Properties.Settings.Default.maxDepth;
            max_depth_value.ToolTip = "Maximum depth of recursion higher=more routes";
            max_jumps_value.Value = Properties.Settings.Default.maxJumps;
            max_jumps_value.ToolTip = "Maximum number of jumps per route";
            best_count_value.Value = Properties.Settings.Default.bestCount;
            best_count_value.ToolTip = "How many routes you want to have in the final Top Routes output";
            warning_max_value.Value = Properties.Settings.Default.warningMaxWeight;
            warning_max_value.ToolTip = "Maxiumum warning points before a route will be ignored. Lower = More shown routes";
            max_undersells_value.Value = Properties.Settings.Default.maxAllowedUndersells;
            max_undersells_value.ToolTip = "How many undersells allowed in a route";
            min_sell_req_distance_value.Value = Properties.Settings.Default.minRequiredSellDist;
            min_sell_req_distance_value.ToolTip = "Minimum distance to sell items LY";
            max_goods_overflow_value.Value = Properties.Settings.Default.maxGoodsBeforeOverflow;
            max_goods_overflow_value.ToolTip = "Limits the number of unique goods you have to sell in one single station before the route gets marked with an overflow warning";
            AlwaysonTop.IsChecked = Properties.Settings.Default.alwaysOnTop;

        }

        private void setPreferences()
        {
            Properties.Settings.Default.lastSystem = starting_system_value.SelectedIndex;
            Properties.Settings.Default.maxStationDist = (int)max_stn_value.Value;
            Properties.Settings.Default.maxJumpDist = (int)max_jump_value.Value;
            Properties.Settings.Default.minSellDist = (int)min_sell_distance_value.Value;
            Properties.Settings.Default.maxDepth = (int)max_depth_value.Value;
            Properties.Settings.Default.maxJumps = (int)max_jumps_value.Value;
            Properties.Settings.Default.bestCount = (int)best_count_value.Value;
            Properties.Settings.Default.warningMaxWeight = (int)warning_max_value.Value;
            Properties.Settings.Default.maxAllowedUndersells = (int)max_undersells_value.Value;
            Properties.Settings.Default.minRequiredSellDist = (int)min_sell_req_distance_value.Value;
            Properties.Settings.Default.maxGoodsBeforeOverflow = (int)max_goods_overflow_value.Value;
            Properties.Settings.Default.Save();
        }

        public void refreshSystemList()
        {
            starting_system_value.Items.Clear();
            please_wait.Visibility = System.Windows.Visibility.Visible;
            control_grid.Visibility = System.Windows.Visibility.Hidden;
            DoEvents();
            System.Threading.Thread thread = new System.Threading.Thread(setSystemList);
            thread.IsBackground = true;
            thread.Start();
            while (thread.IsAlive)
            {
                DoEvents();
                System.Threading.Thread.Sleep(100);
            }
            setControlList();
            please_wait.Visibility = System.Windows.Visibility.Hidden;
            control_grid.Visibility = System.Windows.Visibility.Visible;            
        }


        public void setControlList()
        {
            List<string> listSystems = new List<string>();
            if (!File.Exists(SystemList))
            {
                MessageBox.Show("ERROR: No System List found on your system. Press 'Refresh System List' and try again.");
                return;
            }
            
            DoEvents();
            foreach (string system in File.ReadAllLines(SystemList))
            {
                listSystems.Add(system);
                
            }
            var orderedList = listSystems.OrderBy(i => i);
            foreach (var item in orderedList)
            {
                starting_system_value.Items.Add(item);
            }

          
        }

        public void DoEvents()
        {
            DispatcherFrame frame = new DispatcherFrame();
            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background,
                new DispatcherOperationCallback(ExitFrame), frame);
            Dispatcher.PushFrame(frame);
        }

        public object ExitFrame(object f)
        {
            ((DispatcherFrame)f).Continue = false;
            return null;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            refreshSystemList();
            starting_system_value.SelectedIndex = starting_system_value.Items.Count - 1;
        }

        private void close_button_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

        private void minimize_button_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = System.Windows.WindowState.Minimized;
        }

        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void AlwaysonTop_Checked(object sender, RoutedEventArgs e)
        {
            this.Topmost = true;
            Properties.Settings.Default.alwaysOnTop = AlwaysonTop.IsChecked??false;
            Properties.Settings.Default.Save();
        }

        private void AlwaysonTop_Unchecked(object sender, RoutedEventArgs e)
        {
            this.Topmost = false;
            Properties.Settings.Default.alwaysOnTop = AlwaysonTop.IsChecked ?? false;
            Properties.Settings.Default.Save();
        }

        private void linkLabel1_Click(object sender, RoutedEventArgs e)
        {
            linkLabel1.Content = "Checking...";
            updateThreaded(false);
        }

        private void ruby_update_Click(object sender, RoutedEventArgs e)
        {
            control_grid.Visibility = System.Windows.Visibility.Hidden;
            please_wait_Ruby.Visibility = System.Windows.Visibility.Visible;
            DoEvents();
            
            WebClient web = new WebClient();
            web.DownloadFileCompleted+= new System.ComponentModel.AsyncCompletedEventHandler(downloadComplete);
            web.DownloadFileAsync(new Uri("https://raw.githubusercontent.com/cowboy/ed-rare-trade-route-generator/master/generate-routes.rb"), tmpRubyFile);
            DoEvents();
        }

        void downloadComplete(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            
            if (e.Error != null)
                MessageBox.Show(e.Error.Message);
            else
                replaceScript();

            control_grid.Visibility = System.Windows.Visibility.Visible;
            please_wait_Ruby.Visibility = System.Windows.Visibility.Hidden;
            File.Delete("tmp.dat");
            File.Delete("generate-routes.tmp");
            
        }

        private static string writeDirectory = System.IO.Path.Combine(Environment.CurrentDirectory, "Ruby\\bin");
        private static string defaultScript = System.IO.Path.Combine(writeDirectory, "generate-routes.rb");

        private void replaceScript()
        {            
            using (StreamWriter ruby = new StreamWriter("tmp.dat", false))
            {
                foreach (string line in File.ReadAllLines(tmpRubyFile))
                {
                    string[] items = line.Split('=');
                    if (items.Length > 1)
                    {
                        switch (items[0].ToLower().Trim())
                        {
                            case "$max_station_dist":
                                break;
                            case "$max_jump_dist":
                                break;
                            case "$min_sell_dist":
                                break;
                            case "$max_depth":
                                break;
                            case "$max_jumps":
                                break;
                            case "$best_count":
                                break;
                            case "$warning_max_weight":
                                break;
                            case "$max_allowed_undersells":
                                break;
                            case "$min_required_sell_dist":
                                break;
                            case "$max_goods_before_overflow":
                                break;
                            case "$start_system":
                                break;
                            case "$output_dir":
                                ruby.WriteLine(line);
                                ruby.WriteLine("CONTENTPLACEHOLDER");
                                break;
                            default:
                                ruby.WriteLine(line);
                                break;
                        }
                    }
                    else
                        ruby.WriteLine(line);
                }
            }
            try
            {
                File.Copy("tmp.dat", defaultScript, true);
                File.Delete(System.IO.Path.Combine(rubyWorkingPath, "ED_RareGoods_SystemsDistance - CURRENT.csv"));
                System.Threading.Thread downloadFile = new System.Threading.Thread(() =>
                {
                    using (WebClient wc = new WebClient())
                    {
                        wc.DownloadFile("https://docs.google.com/feeds/download/spreadsheets/Export?key=1haUVaFIxFq5IPqZugJ8cfCEqBrZvFFzcA-uXB4pTfW4&exportFormat=csv&gid=0", System.IO.Path.Combine(rubyWorkingPath, "ED_RareGoods_SystemsDistance - CURRENT.csv"));
                    }
                });
                downloadFile.IsBackground = true;
                downloadFile.Start();
                while (downloadFile.IsAlive)
                {
                    DoEvents();
                    System.Threading.Thread.Sleep(100);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void updateThreaded(bool auto)
        {
            System.Threading.Thread thread = new System.Threading.Thread(() => updateProgram(auto));
            thread.IsBackground = true;
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
        }

        private void updateProgram(bool auto)
        {
            try
            {
                string downloadUrl = "";
                Version newVersion = null;
                string aboutUpdate = "";
                string xmlUrl = hostURL + "/latestVersion.xml";
                XmlTextReader reader = null;
                try
                {
                    reader = new XmlTextReader(xmlUrl);
                    reader.MoveToContent();
                    string elementName = "";
                    if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == "appinfo"))
                    {
                        while (reader.Read())
                        {
                            if (reader.NodeType == XmlNodeType.Element)
                            {
                                elementName = reader.Name;
                            }
                            else
                            {
                                if ((reader.NodeType == XmlNodeType.Text) && (reader.HasValue))
                                    switch (elementName)
                                    {
                                        case "version":
                                            newVersion = new Version(reader.Value);
                                            break;
                                        case "url":
                                            downloadUrl = reader.Value;
                                            break;
                                        case "about":
                                            aboutUpdate = reader.Value;
                                            break;
                                    }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    File.AppendAllText("Error.log", ex.Message);
                    return;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
                Version applicationVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
                if (applicationVersion.CompareTo(newVersion) < 0 || Program.forceUpdate)
                {
                    string str = String.Format("New version found!\nYour version: {0}.\nNewest version: {1}. \n\nAdded in this version: \n{2}. ", applicationVersion, newVersion, aboutUpdate);
                    if (System.Windows.Forms.MessageBox.Show(str + "\n\nWould you like to download this update?\n", "Check for updates", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        try
                        {

                            Dispatcher.Invoke(new Action(() => createDownloadWindow(downloadUrl)));

                        }
                        catch { }
                        return;
                    }
                }
                else
                {
                    if (!auto)
                        System.Windows.Forms.MessageBox.Show("Your version: " + applicationVersion + "  is up to date.", "Check for Updates", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.None);
                }
            }
            catch (Exception ex)
            {
                File.AppendAllText("Error.log", ex.Message);
            }


            linkLabel1.Dispatcher.Invoke(new Action(() => { linkLabel1.Content = "Check For Update"; }));
        }
        private void createDownloadWindow(string downloadUrl)
        {
            update updater = new update();
            updater.url = downloadUrl;
            updater.Owner = this;
            updater.WindowStartupLocation = WindowStartupLocation.CenterOwner;

            updater.ShowDialog();
            this.Close();
        }

        
    }
}
