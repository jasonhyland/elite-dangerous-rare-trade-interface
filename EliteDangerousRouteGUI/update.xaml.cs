﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net;
using System.ComponentModel;

namespace EliteDangerousRouteGUI
{
    /// <summary>
    /// Interaction logic for update.xaml
    /// </summary>
    public partial class update : Window
    {
        public string url;
        public update()
        {
            try
            {
                InitializeComponent();
            }
            catch(Exception ex)
            {
            }
        }

        private void startUpdate()
        {
            WebClient webClient = new WebClient();
            webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(Completed);
            webClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(ProgressChanged);
            webClient.DownloadFileAsync(new Uri(url), @"latest_update.exe");
        }

        private void ProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
        }

        private void Completed(object sender, AsyncCompletedEventArgs e)
        {
            System.Windows.MessageBox.Show("Download completed! Press OK to update and relaunch the program!");
            
            if (!File.Exists("AutoUpdate.exe") && File.Exists("latest_update.exe"))
            {
                System.Threading.Thread.Sleep(1000);
                string argument = string.Format("-o\"{0}\" -y", Environment.CurrentDirectory);
                ProcessStartInfo pinfo = new ProcessStartInfo("latest_update.exe", argument);
                pinfo.WorkingDirectory = Environment.CurrentDirectory;
                pinfo.WindowStyle = ProcessWindowStyle.Hidden;
                pinfo.CreateNoWindow = true;

                Process pro = Process.Start(pinfo);
                while (!pro.HasExited)
                {
                    System.Threading.Thread.Sleep(100);
                }
            }
            try
            {
                System.Diagnostics.Process.Start("AutoUpdate.exe");
                this.Close();
                System.Windows.Application.Current.Shutdown();
            }
            catch
            {
                System.Windows.MessageBox.Show("There was an error upgrading. Please try again!", "Error Upgrading", System.Windows.MessageBoxButton.OK, MessageBoxImage.Error);
                this.Close();
            }

        }

        private void update_Load(object sender, EventArgs e)
        {
            startUpdate();
        }
    }
}
  